from fol4l_distance import Dataset, FOL4LDistance
from sklearn.neighbors import KNeighborsClassifier
import numpy as np

data = Dataset.read_file('dataset1.pl', 'dataset1_id.csv')
m = FOL4LDistance(data)
print('D(atom1, atom2) = %.4f' % m.dist('atom1', 'atom2'))
print('D(atom1, atom3) = %.4f' % m.dist('atom1', 'atom3'))

print(m.pairwise_distances(['atom1', 'atom2', 'atom3'], 
    ['atom1', 'atom2', 'atom3']))
