import numpy as np
import sympy
import math
import sys
import fol4l_distance.utils

class Node:
    @staticmethod
    def l0_node(parent, p, args):
        # x and y are IDs
        x, y = args
        return Node(parent, 0, 'D(%s, %s)' % (x, y), p, args)

    @staticmethod
    def l1_node(parent, p, args):
        # x and y are IDs
        x, y = args
        return Node(parent, 1, 'D_%s(%s, %s)' % (p, x, y), p, args)

    @staticmethod
    def l2_node(parent, p, args):
        # x and y are lists of arguments
        x, y = args
        return Node(parent, 2, 'd_%s(%s, %s)' % (p, str(x), str(y)), p, args)

    @staticmethod
    def l3_node(parent, value):
        n = Node(parent, 3, str(value), '', [])
        n.value = value
        n.has_value = True
        return n

    def __init__(self, parent, n_type, n_name, pred_symb, args):
        self.parent = parent
        self.n_type = n_type
        self.n_name = n_name
        self.pred_symb = pred_symb
        self.args = args
        self.visited = False
        self.value = 0.0
        self.has_value = False
        self.children = []
        self.nx = 0
        self.ny = 0
        self.cycle = False
        self.eq = ''

    def expand(self, dataset):
        if self.n_type == 0:
            if not self.cycle:
                for p in self.pred_symb:
                    n = Node.l1_node(self, p, self.args)
                    self.children.append(n)
        elif self.n_type == 1:
            x, y = self.args
            if (self.pred_symb not in dataset.data[x] 
                    and self.pred_symb not in dataset.data[y]):
                self.value = 0.0
                self.has_value = True
            elif (self.pred_symb not in dataset.data[x] 
                    and self.pred_symb in dataset.data[y]):
                self.value = 1.0
                self.has_value = True
            elif (self.pred_symb in dataset.data[x] 
                    and self.pred_symb not in dataset.data[y]):
                self.value = 1.0
                self.has_value = True
            else:
                xs = dataset.data[x][self.pred_symb]
                ys = dataset.data[y][self.pred_symb]
                self.nx = len(xs)
                self.ny = len(ys)
                for i in xs:
                    for j in ys:
                        n = Node.l2_node(self, self.pred_symb, [i, j])
                        self.children.append(n)
        elif self.n_type == 2:
            xs, ys = self.args
            for x, y, m, d, r in zip(xs, ys, 
                    dataset.pred_symb[self.pred_symb].mode,
                    dataset.diff[self.pred_symb],
                    dataset.pred_symb[self.pred_symb].pred_rel):
                if x == y:
                    n = Node.l3_node(self, 0.0)
                else:
                    if m == 'C':
                        n = Node.l3_node(self, 1.0)
                    elif m == 'R':
                        n = Node.l3_node(self, 
                                ((float(x) - float(y))/d)**2.0)
                    elif m == 'I':
                        n = Node.l0_node(self, r, sorted([x, y]))
                        if self.has_parent(n) is not None:
                            n.cycle = True
                self.children.append(n)
        return self.children

    def solve(self):
        p = self.parent
        self.eq = 'x'
        eq = ''
        while True:
            if p.n_type == 2:
                l = []
                for n in p.children:
                    if n.n_type == 3:
                        l.append(str(n.value))
                    else:
                        if n.cycle and n != self:
                            print('Found a cycle with multiple variables', 
                                  file=sys.stderr)
                            exit()
                        else:
                            l.append('(' + n.eq + ')**2')
                p.eq = 'sqrt((%s)/%d)' % ('+'.join(l), len(p.children))
                p.parent.eq = p.eq
            elif p.n_type == 0:
                if p.n_name == self.n_name:
                    l = []
                    for n in p.children:
                        l.append('(' + n.eq + ')**2')
                    p.eq = 'x - sqrt((%s)/%d)' % ('+'.join(l), 
                            len(p.children))
                    eq = p.eq
                    break
                else:
                    l = []
                    for n in p.children:
                        l.append('(' + n.eq + ')**2')
                    p.eq = 'sqrt((%s)/%d)' % ('+'.join(l), len(p.children))
            p = p.parent
        x = sympy.symbols('x')
        return sympy.solve(eq, x)[0]

    def collect(self, cache):
        if self.n_type == 0:
            if not self.cycle:
                s = 0.0
                for n in self.children:
                    s += n.value ** 2
                s /= len(self.children)
                self.value = math.sqrt(s)
                cache[self.n_name] = self.value
            else:
                self.value = self.solve()
            return self.value
        elif self.n_type == 1:
            if self.has_value:
                return self.value
            else:
                mat = np.empty((self.nx, self.ny))
                i = 0
                j = 0
                for n in self.children:
                    mat[i, j] = n.value
                    j = (j + 1) % self.ny
                    if j == 0:
                        i += 1
                m1 = np.amax(np.amin(mat, axis=1))
                m2 = np.amax(np.amin(mat, axis=0))
                self.value = max(m1, m2)
                return self.value
        elif self.n_type == 2:
            s = 0.0
            for n in self.children:
                s += n.value ** 2
            s /= len(self.children)
            self.value = math.sqrt(s)
            return self.value
        else:
            return self.value

    def has_parent(self, new_node):
        p = self.parent
        while p is not None:
            if p.n_name == new_node.n_name:
                return p
            p = p.parent
        return None

    def __str__(self):
        return self.n_name

    def __repr__(self):
        return str(self)


class FOL4LDistance:
    def __init__(self, dataset):
        self.dataset = dataset
        self.cache = dict()

    def dist(self, x, y):
        if x == y:
            return 0.0

        n = Node.l0_node(None, self.dataset.pred_main, sorted([x, y]))

        if n.n_name in self.cache:
            return self.cache[n.n_name]

        frontier = [n]

        while len(frontier) > 0:
            node = frontier[-1]
            if not node.visited:
                for n in node.expand(self.dataset):
                    frontier.append(n)
                node.visited = True
            else:
                node = frontier.pop()
                node.collect(self.cache)
        return node.value

    def cdist(self, X, Y):
        d = np.empty((len(X), len(Y)))
        for i, x in enumerate(X):
            for j, y in enumerate(Y):
                d[i, j] = self.dist(x, y)
        return d
