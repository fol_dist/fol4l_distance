import re
import sys
import numpy as np
import sympy
import csv


class Atom:
    def __init__(self, pred_symb, atom_id, args):
        self.pred_symb = pred_symb      # predicate symbol
        self.atom_id = atom_id          # atom id
        self.args = args                # list of arguments

    atom_pat = re.compile('^([a-z]\w*)\(("?.+"?(("?,(.+)"?)*)?)\)\.')
    splt_pat = re.compile(''',\s*(?=(?:[^'"]|'[^']*'|"[^"]*")*$)''')

    @staticmethod
    def parse_atom(line):
        res = Atom.atom_pat.match(line)
        if res is not None:
            pred_symb = res.group(1)
            args = Atom.splt_pat.split(res.group(2))
            atom_id = args[0]
            args = args[1:]
            return Atom(pred_symb, atom_id, args)
        return None

class PredicateSymbol:
    def __init__(self, pred_symb, pred_rel, mode):
        self.pred_symb = pred_symb      
        self.pred_rel = pred_rel       
        self.mode = mode              

    def __str__(self):
        return '(' + str(self.pred_symb) + ':' + str(self.pred_rel) + \
               ':' + str(self.mode) + ')'

    def __repr__(self):
        return str(self)


class Dataset:
    def __init__(self, data, diff, symb, psmn, main_ids):
        self.data = data
        self.diff = diff
        self.pred_symb = symb
        self.pred_main = psmn    # predicate symbols for main ids
        self.main_ids = main_ids

    def __str__(self):
        return '(' + str(self.data) + ':' + str(self.diff) + ':' + \
               str(self.pred_symb) + ':' + str(self.pred_main) + ')'

    def __repr__(self):
        return str(self)

    @staticmethod
    def read_file(f_data, f_id):
        comt_pat = re.compile('^[%!]')
        data = dict()
        mode = dict()
        atms = dict()
        maxv = dict()
        minv = dict()

        f = open(f_data, 'r')
        l_no = 0
        for line in f:
            l_no += 1
            line = line.strip()
            if comt_pat.match(line) is not None or len(line) == 0:
                continue
            atom = Atom.parse_atom(line)
            if atom is None:
                print("Syntax error while processing %s line %d" % (f_data, 
                    l_no), file=sys.stderr)
                return None
            if atom.pred_symb not in mode:
                mode[atom.pred_symb] = ['R'] * len(atom.args)
            if atom.pred_symb not in maxv:
                maxv[atom.pred_symb] = [-sympy.oo] * len(mode[atom.pred_symb])
                minv[atom.pred_symb] = [+sympy.oo] * len(mode[atom.pred_symb])
            if atom.pred_symb not in atms:
                atms[atom.pred_symb] = []
            for i, a in enumerate(atom.args):
                if not is_float(a):
                    mode[atom.pred_symb][i] = 'C'
                    maxv[atom.pred_symb][i] = -sympy.oo
                    minv[atom.pred_symb][i] = +sympy.oo
                elif is_float(a) and mode[atom.pred_symb][i] == 'R':
                    v = float(atom.args[i])
                    if v > maxv[atom.pred_symb][i]:
                        maxv[atom.pred_symb][i] = v
                    if v < minv[atom.pred_symb][i]:
                        minv[atom.pred_symb][i] = v
            atms[atom.pred_symb].append(atom)
            if atom.atom_id not in data:
                data[atom.atom_id] = dict()
            if atom.pred_symb not in data[atom.atom_id]:
                data[atom.atom_id][atom.pred_symb] = []
            data[atom.atom_id][atom.pred_symb].append(atom.args)

        pred_symb = dict()
        for p in atms.keys():
            isid = [1] * len(mode[p])
            pred_rel = [set()] * len(mode[p])
            for a in atms[p]:
                for i, g in enumerate(a.args):
                    if mode[p][i] == 'R':
                        isid[i] = 0
                        pred_rel[i] = set()
                    else:
                        if g in data:
                            pred_rel[i] = pred_rel[i].union(
                                            set(data[g].keys()))
                        else:
                            isid[i] = 0
            for i, j in enumerate(isid):
                if j == 1:
                    mode[p][i] = 'I'
            pred_symb[p] = PredicateSymbol(p, pred_rel, mode[p])

        diff = dict()
        for k in maxv:
            diff[k] = np.array(maxv[k]) - np.array(minv[k])

        if type(f_id) is list:
            main_ids = f_id
        else:
            main_ids = []
            h = csv.reader(open(f_id, 'r'))
            for r in h:
                main_ids.append(r[0])

        pred_main = set()
        for i in main_ids:
            pred_main = pred_main.union(set(data[i].keys()))

        return Dataset(data, diff, pred_symb, pred_main, main_ids)

def is_float(value):
    try:
        float(value)
        return True
    except ValueError:
        return False
