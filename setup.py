from setuptools import setup

setup(name='fol4l_distance', 
      version='0.1', 
      description='Distance function for First-Order Logic objects',
      url='https://bitbucket.org/fol-dist/fol4l_distance',
      author='Nirattaya Khamsemanan, Cholwich Nattee, and Masayuki Numao',
      author_email='cholwich@gmail.com',
      license='MIT',
      packages=['fol4l_distance'],
      install_requires=[
          'numpy', 'sympy'
      ],
      zip_safe=False)
